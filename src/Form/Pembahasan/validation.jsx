import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import * as Validator from "validatorjs";

const Input = ({ type, name, placeholder, onChange }) => {
  return (
    <div>
      <Form.Control
        type={type}
        placeholder={placeholder}
        name={name}
        onChange={(e) => onChange(e.target.value)}
      />
    </div>
  );
};

const ShowErrors = ({ errors }) => {
  return (
    <div style={{ color: "red", marginLeft: "-20px" }}>
      {errors.map((errors, i) => (
        <h6 key={i}>{errors}</h6>
      ))}
    </div>
  );
};

export default class Validation extends React.Component {
  state = {
    email: "",
    password: "",
    kelas:"",
    errors: [],
  };

  handleSubmit = (event) => {
    event.preventDefault();
    // console.log(this.state);
    const { email, password, kelas } = this.state;

    let data = { email, password, kelas };

    let rules = {
      email: "required|email",
      password: "min:8|required",
      kelas: "required",
    };

    let validation = new Validator(data, rules);
    // validation.passes();
    // this.setState({
    //   errors: [
    //     ...validation.errors.get("email"),
    //     ...validation.errors.get("password"),
    //   ],
    // });

    if (validation.passes() > 0) {
      alert(`
        Email: ${this.state.email}
        Password: ${this.state.password}
        Kelas: ${this.state.kelas}
        `);
    } else {
      this.setState({
        errors: [
          ...validation.errors.get("email"),
          ...validation.errors.get("password"),
          ...validation.errors.get("kelas"),
        ],
      });
    }
  };

  render() {
    return (
      <Container className="mt-5">
        <Row>
          <Col></Col>
          <Col xs={6}>
            <Form onSubmit={this.handleSubmit}>
              <Form.Group className="mb-3">
                <Form.Label>Email address</Form.Label>
                <Input
                  type="email"
                  placeholder="Email"
                  onChange={(value) => this.setState({ email: value })}
                />
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label>Password</Form.Label>
                <Input
                  type="password"
                  placeholder="Password"
                  onChange={(value) => this.setState({ password: value })}
                />
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label>Pilih Kelas</Form.Label>
                <Form.Select name="kelas"
                  onChange={ e=> this.setState({kelas: e.target.value})}>
                  <option>--Pilih Kelas--</option>
                  <option value="Programmer - Laravel">Programmer - Laravel</option>
                  <option value="Programmer - Mern">Programmer - Mern</option>
                  <option value="UI/UX Designer">UI/UX Designer</option>
                </Form.Select>
              </Form.Group>
              {this.state.errors && <ShowErrors errors={this.state.errors} />}
              <Button variant="primary" type="submit">
                Submit
              </Button>
            </Form>
          </Col>
          <Col></Col>
        </Row>
      </Container>
    );
  }
}
